package com.matchjoeyshirin

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

/**
 * Assignment 1:  Image Match Game
 *
 *  class for the scores activity displaying all the counts
 *  like total games played
 *
 * @author Joey Guedes
 * @author Shirin Eskandari
 * @version 2019-10-07
 **/


class ScoreActivity : AppCompatActivity() {

    private lateinit var gamesWonText : TextView
    private lateinit var missesText : TextView
    private lateinit var playedText : TextView
    /**
     * @author Shirin Eskandari
     * @author Joey Guedes
     * activity's onCreate method. initializes the textviews and sets their text
     * to the counts gotten from the sharedPref instance
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)
        gamesWonText = findViewById(R.id.score_hits)
        missesText = findViewById(R.id.score_misses)
        playedText = findViewById(R.id.score_countOfGame)

        val sharedPref = getSharedPreferences("counts", Context.MODE_PRIVATE)
        val gamesWon = sharedPref.getInt(getString(R.string.sharedWins), 0)
        val missedHits = sharedPref.getInt(getString(R.string.sharedMisses), 0)
        val gamesPlayed = sharedPref.getInt(getString(R.string.sharedGames), 0)

        val winsString = getString(R.string.score_hits)
        val missedString = getString(R.string.score_misses)
        val gamesString = getString(R.string.score_countOfGame)

        gamesWonText.text = "$winsString $gamesWon"
        missesText.text = "$missedString $missedHits"
        playedText.text = "$gamesString $gamesPlayed"

    }
}
