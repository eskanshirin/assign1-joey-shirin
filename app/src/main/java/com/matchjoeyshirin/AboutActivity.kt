package com.matchjoeyshirin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

/**
 * Assignment 1:  Image Match Game
 *
 *  this is the about activity, simply explaining how to play the game,
 *  showing the credits, and showing a picture of our faces
 *
 * @author Joey Guedes
 * @author Shirin Eskandari
 * @version 2019-10-06
 **/




class AboutActivity : AppCompatActivity() {

    private lateinit var shirinPhoto : ImageView
    private lateinit var joeyPhoto : ImageView
    /**
     * @author Joey Guedes
     * @author Shirin Eskandari
     * onCreate for the about activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        joeyPhoto = findViewById(R.id.imagejoey)
        shirinPhoto = findViewById(R.id.imageshirin)
        joeyPhoto.setScaleType(ImageView.ScaleType.CENTER)
        shirinPhoto.setScaleType(ImageView.ScaleType.CENTER)
        joeyPhoto.adjustViewBounds = true
        shirinPhoto.adjustViewBounds = true
        joeyPhoto.setImageResource(R.drawable.joeyimage)
        shirinPhoto.setImageResource(R.drawable.shirinimage)
    }
}
