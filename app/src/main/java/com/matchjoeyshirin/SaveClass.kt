package com.matchjoeyshirin


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

// had to turn on experimental for this

/**
 * Assignment 1:  Image Match Game
 *
 *  a class that implements Parcelable to save all the necessary info in a bundle
 *  saves the games won, the missed hits, the position of the images,
 *  if the images are black and white, if the images are disabled, the outlier
 *  and the total games played (during that lifecycle)
 *
 * @author Joey Guedes
 * @author Shirin Eskandari
 * @version 2019-10-07
 **/


@Parcelize
data class SaveClass(val matchCount : Int, val missedHits : Int, val images : List<Int>,
                     val blackAndWhite : List<Boolean>, val disabled : List<Boolean>,
                     val gamesPlayed : Int, val outlier : Int) : Parcelable