package com.matchjoeyshirin

import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.view.View
import com.matchjoeyshirin.R.drawable.*
import android.content.Intent
import android.widget.Toast
import android.app.AlertDialog
import android.content.Context
import android.util.Log


/**
 * Assignment 1:  Image Match Game
 *
 *  this is the main activity that appears when you start the game,
 *  along with the actual game logic
 *
 * @author Joey Guedes
 * @author Shirin Eskandari
 * @version 2019-10-07
 **/


class MainActivity : AppCompatActivity() {
    private lateinit var img1: ImageButton
    private lateinit var img2: ImageButton
    private lateinit var img3: ImageButton
    private lateinit var img4: ImageButton
    private lateinit var img5: ImageButton
    private lateinit var img6: ImageButton
    private lateinit var countText : TextView
    private lateinit var missedText : TextView
    private lateinit var playedText : TextView
    private var imgButtonList : List<ImageButton> = listOf<ImageButton>()
    private var imgButtonResourceList : List<Int> = listOf<Int>()
    internal var winCount = 0 // wins
    internal var missedCount = 0 // misses
    internal var totalMisses = 0
    internal var gamesPlayed = 0 // total games played
    internal var outlier : Int = 0
    internal var randnum : Int = 0
    internal var gameEnded : Int = 0
    private val TAG = "MainActivity"

    /**
     * The onCreate function that initializes all the necessary ui controls for later use.
     * calls the necessary methods to initialize a game state. also sets up the text with the
     * different counts.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        img1 = findViewById(R.id.imageButton1) as ImageButton
        img2 = findViewById(R.id.imageButton2) as ImageButton
        img3 = findViewById(R.id.imageButton3) as ImageButton
        img4 = findViewById(R.id.imageButton4) as ImageButton
        img5 = findViewById(R.id.imageButton5) as ImageButton
        img6 = findViewById(R.id.imageButton6) as ImageButton
        countText = findViewById(R.id.score) as TextView
        missedText = findViewById(R.id.missedHits) as TextView
        playedText = findViewById(R.id.totalGames) as TextView
        val missedString = getString(R.string.missedHits)
        val countString = getString(R.string.score)
        val playedString = getString(R.string.totalGames)
        countText.text = "$countString $winCount"
        missedText.text = "$missedString $missedCount"
        playedText.text = "$playedString $gamesPlayed"
        // this is all to make sure the pics can fit during testing, this'll get fixed in ui
        img1.setScaleType(ImageView.ScaleType.CENTER)
        img2.setScaleType(ImageView.ScaleType.CENTER)
        img3.setScaleType(ImageView.ScaleType.CENTER)
        img4.setScaleType(ImageView.ScaleType.CENTER)
        img5.setScaleType(ImageView.ScaleType.CENTER)
        img6.setScaleType(ImageView.ScaleType.CENTER)
        img1.setAdjustViewBounds(true)
        img2.setAdjustViewBounds(true)
        img3.setAdjustViewBounds(true)
        img4.setAdjustViewBounds(true)
        img5.setAdjustViewBounds(true)
        img6.setAdjustViewBounds(true)
        // end setting stuff for testing
        imgButtonList = listOf(img1, img2, img3, img4, img5, img6)
        randnum = (0..1).random()
        // shuffles the images around before setting their images and setting the outlier tag
        setResourceList()
        imgButtonResourceList = imgButtonResourceList.shuffled()
        resetTags(imgButtonList)
        setImages(imgButtonList)
        setOutlier(imgButtonList)
    }


    /**
     * @author Joey Guedes
     * main game logic. gets the imagebutton that was clicked, then does the logic like if it's
     * the outlier, update the necessary counts and whatnot. disabled all
     * images on win or lose. also pops up dialogues based on if they won or lost.
     */
    fun checkIfOutlier(v : View) {
        val playedString = getString(R.string.totalGames)
        val clicked = findViewById(v.id) as ImageButton
        if (clicked.tag == 30) {
            changeImagesIfGameEnds(imgButtonList)
            removeClickable(imgButtonList)
            val countString = getString(R.string.score)
            winCount++
            gamesPlayed++
            gameEnded = 1
            saveCounts()
            countText.text = "$countString $winCount"
            playedText.text = "$playedString $gamesPlayed"
            popupDialogue("You win! Click on Reset to play again")
        }
        else {
            val missedString = getString(R.string.missedHits)
            missedCount++
            totalMisses++
            saveCounts()
            missedText.text = "$missedString $missedCount"
            Toast.makeText(this, "Wrong Choice", Toast.LENGTH_SHORT).show()
            if (missedCount == 2) {
                gamesPlayed++
                gameEnded = 1
                saveCounts()
                playedText.text = "$playedString $gamesPlayed"
                removeClickable(imgButtonList)
                changeImagesIfGameEnds(imgButtonList)
                popupDialogue("Game ended, click on Reset to try again")
            }
        }
    }

    /**
     * @author Joey Guedes
     * sets resource list to one of the sets based on a random number
     */
    fun setResourceList() {
        if (randnum == 0) {
            imgButtonResourceList = listOf(bird1, bird2, bird3, bird4, bird5, birdoutlier)
            outlier = birdoutlier
        }
        else {
            imgButtonResourceList = listOf(red1, red2, red3, red4, red5, redoutlier)
            outlier = redoutlier
        }
    }

    /**
     * @author Joey Guedes
     * @param list of image buttons
     * sets the images of the image button list using the shuffled resource list
     */
    fun setImages(li : List<ImageButton>) {
        val x : MutableList<Int> = mutableListOf()
        for ((index,element) in li.withIndex()) {
            element.setImageResource(imgButtonResourceList[index])
        }
        for (element in imgButtonList) {
            x.add(element.tag as Int)
        }
        Log.d(TAG, "setImages tags $x")

    }

    /**
     * @author Joey Guedes
     * @param list of image buttons
     * gets the int id of the outlier image, then compares the image resource list index with the
     * id. if it matches, sets the tag of the respective image button arbitrarily to 30, making it
     * the outlier among the others that have tag 0
     */
    fun setOutlier(li : List<ImageButton>) {
        for ((index, element) in li.withIndex()) {
            if (imgButtonResourceList[index] == outlier) {
                element.tag = 30
            }
        }
        val x : MutableList<Int> = mutableListOf()
        for (element in imgButtonList) {
            x.add(element.tag as Int)
        }
        Log.d(TAG, "setOutlier tags $x")
        Log.d(TAG, "setOutlier resourceList $imgButtonResourceList outlierImage $outlier")
    }

    /**
     * @author Joey Guedes
     * event handler for the reset button. resets the missed hits count and text, shuffles the
     * resource list, then calls the other intialization methods. also removes the black and white
     * filters and re-enables the images to be clicked
     */

    fun onResetClick(v: View) {
        if (gameEnded == 0) {
            gamesPlayed++
            saveCounts()
            val playedString = getString(R.string.totalGames)
            playedText.text = "$playedString $gamesPlayed"
        }
        gameEnded = 0
        missedCount = 0
        val missedString = getString(R.string.missedHits)
        missedText.text = "$missedString $missedCount"
        randnum =  (0..1).random()
        setResourceList()
        imgButtonResourceList = imgButtonResourceList.shuffled()
        resetTags(imgButtonList)
        setImages(imgButtonList)
        setOutlier(imgButtonList)
        removeColorFilter(imgButtonList)
        enableClickable(imgButtonList)
    }


    /**
     * @author Joey Guedes
     * @param list of image buttons
     * goes through the list, setting the colorFilter to null so it removes
     * the black and white filter on all of theme
     */

    fun removeColorFilter(li : List<ImageButton>) {
        for (element in li) {
            element.colorFilter = null
        }
    }

    /**
     * @author Joey Guedes
     * @param list of image buttons
     * disables the ability to click the image buttons
     */
    fun removeClickable(li : List<ImageButton>) {
        for (element in li) {
            element.isClickable = false
        }
    }
    /**
     * @author Joey Guedes
     * @param list of image buttons
     * enables the ability to click the image buttons
     */
    fun enableClickable(li : List<ImageButton>) {
        for (element in li) {
            element.isClickable = true
        }
    }
    /**
     * @author Joey Guedes
     * @param list of image buttons
     * sets all tags to 0 so there's no lingering outlier when resetting
     */
    fun resetTags(li : List<ImageButton>) {
        for (element in li) {
            element.tag = 0
        }
    }

    /**
     * @author Joey Guedes
     * uses a SaveClass instance to put a Parcelable object into the bundle.
     * sets up some variables to put into the SaveClass instance
     * see SaveClass for info on what is stored
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        val isItFaded : MutableList<Boolean> = mutableListOf()
        val y : MutableList<Int> = mutableListOf()
        for (element in imgButtonList) {
            y.add(element.tag as Int)
        }
        Log.d(TAG, "on saveInstanceState tags $y")
        for (element in imgButtonList) {
            var x = element.colorFilter
            Log.d(TAG, " onSaveInstanceState colorFilter $x")
            if (element.colorFilter == null) {
                isItFaded.add(false)
            }
            else {
                isItFaded.add(true)
            }
        }
        val disabled : MutableList<Boolean> = mutableListOf();
        for (element in imgButtonList) {
            disabled.add(element.isClickable)
        }
        val turnToList =  isItFaded.toList()
        val allInfo = SaveClass(winCount, missedCount, imgButtonResourceList, turnToList, disabled.toList(), gamesPlayed, randnum)
        outState?.run { putParcelable("info", allInfo) }
        super.onSaveInstanceState(outState)
    }

    /**
     * @author Joey Guedes
     * restores the state of the activity using the bundle saved in onSaveInstanceState
     * restores the color filter, the win count, the miss count, the total games played,
     * the position of the images. then it calls the initialization functions necessary
     */
    override fun onRestoreInstanceState(inState: Bundle) {
        var blackandwhite : ColorMatrix = ColorMatrix()
        blackandwhite.setSaturation(0.toFloat())
        var filter : ColorMatrixColorFilter = ColorMatrixColorFilter(blackandwhite)
        super.onRestoreInstanceState(inState)
        val savedState : SaveClass
        inState.run { savedState = getParcelable<SaveClass>("info")!!}
        winCount = savedState.matchCount
        missedCount = savedState.missedHits
        gamesPlayed = savedState.gamesPlayed
        imgButtonResourceList = savedState.images
        Log.d(TAG, "$imgButtonResourceList")
        val filterImages = savedState.blackAndWhite
        val disabled = savedState.disabled
        for ((index, element) in savedState.images.withIndex()) {
            imgButtonList[index].setImageResource(element)
        }
        Log.d(TAG, "onRestoreInstanceState $filterImages")
        for ((index, element) in filterImages.withIndex()) {
            if (element) {
                imgButtonList[index].colorFilter = filter
            }
        }
        randnum = savedState.outlier
        resetTags(imgButtonList)
        setImages(imgButtonList)
        setOutlier(imgButtonList)
        for ((index, element) in imgButtonList.withIndex()) {
                element.isClickable = disabled[index]
        }
        preserveState()

    }

    /**
     * @author Joey Guedes
     * saves the counts we need in sharedPreferences to a sharedPreference named "counts"
     */
    fun saveCounts() {
        val won = winCount
        val misses = totalMisses
        val played = gamesPlayed
        Log.d(TAG, "saveCounts won=$won misses=$misses played=$played")
        with (getSharedPreferences("counts", Context.MODE_PRIVATE).edit()) {
            putInt(getString(R.string.sharedWins), won)
            putInt(getString(R.string.sharedMisses), misses)
            putInt(getString(R.string.sharedGames), played)
            apply()
        }
    }

    /**
     * @author Joey Guedes
     * sets the textview texts to the counts after restoring the state
     */

    fun preserveState() {
        val matchString = getString(R.string.score)
        val missedString = getString(R.string.missedHits)
        val playedString = getString(R.string.totalGames)
        countText.text = "$matchString $winCount"
        missedText.text = "$missedString $missedCount"
        playedText.text = "$playedString $gamesPlayed"
    }

    /**
     * @author Joey Guedes
     * @param string representing the message to appear in the alert dialog
     * pops up a dialogue based off the message provided
     * gotten off of examples from https://developer.android.com/guide/topics/ui/dialogs#AlertDialog
     *
     */
    fun popupDialogue(message : String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message).setPositiveButton("OK") {
            dialog, id ->
        }
        val dialog = builder.create()
        dialog.show()
    }


    /**
     * @author Joey Guedes
     * @param list of image buttons
     * called when the game ends, whether win or lose
     * sets all images except the outlier to black and white to show which is the outlier
     */
    fun changeImagesIfGameEnds(li : List<ImageButton>) {
        var blackandwhite : ColorMatrix = ColorMatrix()
        blackandwhite.setSaturation(0.toFloat())
        var filter : ColorMatrixColorFilter = ColorMatrixColorFilter(blackandwhite)
        for (element in li) {
            if (element.tag != 30) {
                element.colorFilter = filter
            }
        }
    }

    /**
     * @author Joey Guedes
     * event handler for the zero button
     * resets all counts and their respective textview texts
     */
    fun onZeroClick (v : View) {
        winCount = 0
        missedCount = 0
        gamesPlayed = 0
        totalMisses = 0
        saveCounts()
        val countString = getString(R.string.score)
        val missedString = getString(R.string.missedHits)
        val playedString = getString(R.string.totalGames)
        countText.text = "$countString $winCount"
        missedText.text = "$missedString $missedCount"
        playedText.text = "$playedString $gamesPlayed"
    }
    /**
     * @author Shirin Eskandari
     * about button event handler
     * makes a new AboutActivity
     */
    fun about (view: View){
        val intent = Intent(this, AboutActivity::class.java)
        startActivity(intent)
    }

    /**
     * @author Shirin Eskandari
     * scores button event handler
     * makes a new ScoreActivity
     */
    fun onScoreClick(view: View){
        val intent = Intent(this, ScoreActivity::class.java)
        startActivity(intent)
    }
}
